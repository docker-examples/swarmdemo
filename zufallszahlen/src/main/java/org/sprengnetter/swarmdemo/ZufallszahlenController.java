package org.sprengnetter.swarmdemo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Random;

@Path("/zufallszahl")
@Produces("application/json")
public class ZufallszahlenController {

    @Path("/")
    @GET
    public Integer liefereZufallszahl() {
        return new Random().nextInt(1000000)+1;
    }

}
