package org.sprengnetter.swarmdemo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Path("/zufallswort")
@Produces("application/json")
public class ZufallsworteController {

    private List<String> worte = new ArrayList<>(Arrays.asList("Baum", "Ball", "Geld", "Spielzeug", "Python", "Granate", "Experte", "Honig", "Spiel", "Smartphone"));

    @Path("/")
    @GET
    public String liefereZufallswort() {
        return worte.get(new Random().nextInt(worte.size()));
    }

}
