package org.sprengnetter.swarmdemo;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;
import org.sprengnetter.swarmdemo.services.ZufallswortService;
import org.sprengnetter.swarmdemo.services.ZufallszahlService;
import org.springframework.beans.factory.annotation.Autowired;

@Route
public class MainView extends HorizontalLayout {

    public MainView(@Autowired ZufallszahlService zufallszahlService, @Autowired ZufallswortService zufallswortService) {
        Button zufallszahlButton = new Button("Zufallszahl", e -> Notification.show(zufallszahlService.generiereZufallszahl().toString()));
        Button zufallswortButton = new Button("Zufallswort", e -> Notification.show(zufallswortService.generiereZufallswort()));
        add(zufallszahlButton, zufallswortButton);
    }

}