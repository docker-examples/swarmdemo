package org.sprengnetter.swarmdemo.services;

import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

@Component
public class ZufallszahlService {

    private static final String SERVICE_URL = "http://zufallszahlen:8081/zufallszahl";

    private Client client = ClientBuilder.newClient();

    public Integer generiereZufallszahl() {
        return client
                .target(SERVICE_URL)
                .request(MediaType.APPLICATION_JSON)
                .get(Integer.class);
    }

}