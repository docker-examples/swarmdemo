package org.sprengnetter.swarmdemo.services;

import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

@Component
public class ZufallswortService {

    private static final String SERVICE_URL = "http://zufallsworte:8082/zufallswort";

    private Client client = ClientBuilder.newClient();

    public String generiereZufallswort() {
        return client
                .target(SERVICE_URL)
                .request(MediaType.APPLICATION_JSON)
                .get(String.class);
    }

}
