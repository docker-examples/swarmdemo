package org.sprengnetter.swarmdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwarmdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwarmdemoApplication.class, args);
	}
}
