resource "digitalocean_domain" "gotchadomain-main" {
  name       = "gotcha-app.de"
  ip_address = "127.0.0.1"
}

resource "digitalocean_domain" "gotchadomain-www" {
  name       = "www.gotcha-app.de"
  ip_address = "127.0.0.1"
}

resource "digitalocean_domain" "gotchadomain-docker" {
  name       = "docker.gotcha-app.de"
  ip_address = "${var.DOCKER_REGISTRY_IP}"
}